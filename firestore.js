const admin = require("firebase-admin");

const config = require(process.env.KW_CONFIG_PATH);
const serviceAccount = config.FIRESTORE_ACCOUNT;
const databaseURL = config.FIRESTORE_URL;


function _db() {
	let db;

	_init();

	this.getD = _getD;
	this.getCollectionsD = _getCollectionsD;
	this.setD = _setD;
	this.watchD = _watchD;
	this.watchWithSearch = _watchWithSearch;

	function _init() {
		admin.initializeApp({
			credential: serviceAccount == "ON_FIREBASE" ?
				admin.credential.applicationDefault() : admin.credential.cert(serviceAccount),
			databaseURL
		});
		db = admin.firestore();
	}

	async function _getCollectionsD({
		path
	}) {
		let dataObj = null;
		const snapshot = await db.collection(path).where('_id', '==', "1AOtnqEqIetcB61MTZIq3oRNEPHs_mwKRQBnC5GykjHo").get();
		if (snapshot) {
			dataObj = {};
			snapshot.forEach(doc => {
				dataObj[doc.id] = doc.data();
			});
		} else {
			console.log('Error getting documents', err);
		}
		return dataObj;
	}

	async function _getD({
		path,
		defValue
	}) {
		const doc = await db.doc(path).get();
		const data = doc.exists ? doc.data() : null;
		const s = JSON.stringify(data);
		console.log("_getD", s.length <= 500 ? s : s.substr(0, 500) + " (over 500 chars)");
		return data || defValue;
	}

	async function _setD({
		path,
		value
	}) {
		const doc = await db.doc(path).set(value);
		console.log("_setD: done");
		return true;
	}

	function _watchD(path, cb) {
		db.doc(path)
			.onSnapshot(function (doc) {
				cb(doc.data());
			});
	}

	function _watchWithSearch({
		col,
		key,
		val,
		cb
	}) {
		db.collection(col).where(key, "==", val)
			.onSnapshot(function (doc) {
				cb(doc.data());
			});
	}
}

// export default new _db();
module.exports = new _db();