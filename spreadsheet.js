const config = require(process.env.KW_CONFIG_PATH);
const drive = require('./googledrive');
const db = require("./firestore");

const IDS = config.SPREADSHEETS;
const FS_PATH = config.SPREADSHEETS_FIRESTORE_PATH;
const LOCAL_PATH = config.SPREADSHEETS_LOCAL_PATH;

let csvs = null;

async function _resetSheetD() {
	console.log("### spreadsheet: loading from Google Spreadsheet...");
	const proms = [];
	csvs = {};
	Object.keys(IDS).forEach((key, i) => {
		const id = IDS[key];
		proms.push(drive.authAndGetCsvD(id).then(s => {
			csvs[key] = s;
		}));
	});
	await Promise.all(proms);

	//=== Firesotreへ保存
	await db.setD({
		path: FS_PATH,
		value: {
			date: "" + new Date(),
			value: JSON.stringify(csvs)
		}
	}).then(() => {
		console.log("### spreadsheet: Firestore cache is refreshed!!");
	});

	//=== ローカル保存
	const fs = require('fs');
	return new Promise((resolve, reject) => {
		fs.writeFile(LOCAL_PATH, JSON.stringify(csvs), (err) => {
			if (err) {
				console.log("### spreadsheet: local saving is failed!");
				resolve(false);
			} else {
				console.log("### spreadsheet: local csvs are saved!");
				resolve(true);
			}
		});
	});
}

async function _loadD(mode /*CACHE or FILE*/ ) {
	if (mode == "LOCAL") {
		console.log("### spreadsheet: loading from local...");
		csvs = require(config.SPREADSHEETS_LOCAL_PATH);

	} else if (mode == "CACHE") {
		await db.getD({
			path: FS_PATH
		}).then(obj => {
			if (obj) {
				csvs = JSON.parse(obj.value);
			}
		});
		csvs && console.log("### spreadsheet: loading from Firestore cache...");
	}

	if (!csvs) {
		console.log("### spreadsheet: data not found.");
		return Promise.reject("DATA_NOT_FOUND");
	}
}

function _get(id) {
	return csvs[id];
}

module.exports = {
	resetSheetD: _resetSheetD,
	loadD: _loadD,
	get: _get
}