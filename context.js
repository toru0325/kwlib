function kwContext(sess, onExec) {
    const temp = sess.temp;

    _init();

    this.setDialog = _setDialog;
    this.applyResponses = _applyResponses;
    this.getReprompt = _getReprompt;

    this.exec = _exec;
    this.posTo = _posTo;
    this.jump = _jump;
    this.next = _next;
    this.id = _id;
    this.pos = _pos;

    function _init() {
        !temp.hasOwnProperty("contextId") && (temp.contextId = "");
        !temp.hasOwnProperty("contextPos") && (temp.contextPos = 0);
    }

    /*
     *
     * Dialog (suggestion + reprompt) 
     */
    function _setDialog({
        suggestions,
        reprompts
    }) {
        temp.suggestions = suggestions || null;
        temp.reprompts = reprompts || null;
    }

    function _applyResponses(responses) {
        responses.suggestions = temp.suggestions ? JSON.parse(JSON.stringify(temp.suggestions)) : null;
    }

    function _getReprompt(i) {
        return temp.reprompts[Math.min(i, temp.reprompts.length - 1)];
    }

    /*
     *
     * Context
     */
    function _exec() {
        onExec();
    }

    function _posTo(cPos, isExec) {
        (typeof cPos == "number") && (temp.contextPos = cPos);
        isExec && onExec();
    }

    function _jump(cId, cPos, isExec) {
        (typeof cId == "string") && (temp.contextId = cId);
        temp.contextPos = (typeof cPos == "number") ? cPos : 0;
        isExec && onExec();
    }

    function _next(isExec) {
        temp.contextPos++;
        isExec && onExec();
    }

    function _id() {
        return temp.contextId;
    }

    function _pos() {
        return temp.contextPos;
    }
}

module.exports = kwContext;