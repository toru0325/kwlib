//=== リモートCSVをオブジェクトとして利用

const kw = require("./kw");
const sheet = require("./spreadsheet");

let data = {};

const _init = csvFileId => {
	const csv = sheet.get(csvFileId);
	data = kw.csvToObj(csv);
}

const _getAll = ()=>{
	return data;
}

module.exports = {
	init: _init,
	getAll: _getAll
};
