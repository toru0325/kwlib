// const FILE_ID = "1-MmRfRQ36K-QmFdcMuZOWmdZ3T67EnYtKkxXuRA5K7Q";

const sheet = require("./spreadsheet");
const kw = require("./kw");

let audioUrlBase = "";
let storyData = {};

const _init = (cfg /*{audioUrlBase}*/ ) => {
	({
		audioUrlBase
	} = cfg);
};

const _loadStory = (csvFileId, onLoad) => {
	//=== load story csv via Google Spreadsheet
	const csv = sheet.get(csvFileId);

	// console.log(888888, csv)

	let curId = "";
	const data = kw.csvToObj(csv, (o, id, data) => {
		if (id) {
			curId = id;
		} else {
			data[curId]._subScenes = data[curId]._subScenes || [];
			data[curId]._subScenes.push(o);
		}
	});

	//=== 割り込み改変を許可
	onLoad && onLoad(data);

	for (let k in data) {
		if (storyData[k]) {
			console.log("=== loadStory() Warning === overwitten : " + JSON.stringify(data[k]));
		}
		storyData[k] = data[k];
	}

	// console.log(JSON.stringify(storyData));
};

const _getStory = () => {
	return storyData;
}

function _kwStoryManager({
	execParams,
	// onSay,
	onJump,
	onAsk,
	useSelectPrompt
}) {
	let [cm, sess, data, sb] = execParams;
	const overRepros = {
		"REPROMPT1": null,
		"REPROMPT2": null,
		"REPROMPT_ALEXA": null //=== can be ssml
	};

	this.destroy = _destroy;
	this.exec = _exec;
	this.jump = _jump;
	this.setReprompts = _setReprompts;
	this.sayReprompt = _sayReprompt;
	this.getAlexaReprompt = _getAlexaReprompt;

	_init();

	function _init() {
		//=== storyで使うパラメータをアサイン
		sess.story = sess.hasOwnProperty("story") ? sess.story : {};
		sess.story.curId = sess.story.hasOwnProperty("curId") ? sess.story.curId : "";
		sess.story.valHistory = sess.story.hasOwnProperty("valHistory") ? sess.story.valHistory : {};
	};

	function _destroy() {
		cm = sess = data = sb = null;
	}

	function _say(scene /* コピーの可能性あるのでsceneオブジェクト渡しで */ ) {
		const item = _chooseVariationByKey(scene, "script");

		let text = item["script"];
		const ssml = item["ssml"];

		// text = onSay ? onSay(text) || text : text;

		if (ssml) {
			const ls = ssml.split("\n");
			for (let s of ls) {
				let m;
				if (s.match(/\.mp3$/)) {
					sb.sayAudio2(audioUrlBase + s.replace(/{ID}/g, scene.id).replace(/.mp3/, data.type == "ALEXA" ? "_alexa.mp3" : ".mp3"));
				}else if (m = s.match(/BREAK (.+)$/)) {
					sb.pause(m[1]);
				} else {
					s = (s == "SCRIPT") ? text : s;
					sb.sayHidden(s);
				}
			}
			sb.saySilent(text)
		} else {
			sb.say(text);
		}
	}

	function _chooseVariationByKey(scene, key) { //=== 値が複数でもある場合の被らないようにゲット
		const ls = [];

		scene._subScenes && scene._subScenes.forEach((val) => {
			ls.push(val);
		});

		if (ls.length) {
			ls.push(scene);

			let his = sess.story.valHistory[scene.id + ":" + key];
			(!his) && (sess.story.valHistory[scene.id + ":" + key] = his = []);


			if (his.length >= ls.length) { //=== 残り一回のときに履歴をリフレッシュ
				his.splice(0, his.length - 1);
			}

			const iLs = [];
			for (let i = 0; i < ls.length; i++) {
				(his.indexOf(i) == -1) && iLs.push(i);
			}

			const i = iLs[Math.floor(Math.random() * iLs.length)];

			his.push(i);

			return ls[i];
		} else {
			return scene;
		}
	}

	function _exec() {
		if (!storyData[sess.story.curId]) {
			sb.say("次のストーリーがありません:" + sess.story.curId);
			return;
		}
		if (cm.pos() == 0) { //=== ask
			const dest = _ask();
			if (dest) {
				_jump(dest);
			} else {
				cm.next();
			}

		} else { //=== answer
			const dest = _answer();
			if (dest) {
				_jump(dest);
			}
		}
	}

	function _ask() {
		let scene = storyData[sess.story.curId];
		scene = JSON.parse(JSON.stringify(scene));

		onAsk && onAsk(scene); //=== scene

		// console.log("=== ask ===\n", scene.id);

		const sugg = scene.suggestions ? scene.suggestions.split(",") : [];

		let prompt = "";
		if (useSelectPrompt) {
			prompt = (sugg.length == 1) ? `${sugg[0]}、と言ってください。` :
				(sugg.length == 2) ? `${sugg.join("、または、")}、のどちらにしますか？` :
				(sugg.length > 2) ? `${sugg.join("、または、")}、から選んでください。` : "";
		}

		_say(scene);
		sb.say(prompt);
		sess.temp.prompt = prompt;
		cm.setDialog({
			suggestions: sugg
		});

		return scene.dest || "";
	};

	function _answer() {
		const st = sess.story;
		const scene = storyData[st.curId];

		if (data.text || data.intent) {
			for (let i = 1; i <= 20; i++) {
				const k = "select" + i;
				const sels = scene[k] ? scene[k].replace(/\s/g, "").split(",") : null;
				if (sels) {
					if ((data.text && sels.indexOf(data.text) > -1) || //=== textが空の時は0になってしまうので確認
						sels.indexOf(data.intent) > -1 || sels[0] == "OTHER") {
						const selDest = scene[k + "_dest"] || "";
						if (selDest) { //=== 別のシーンへ
							return selDest;
						} else { //=== 同じシーンで回答繰り返しの場合
							sess.temp.ans && sb.say(sess.temp.ans);
							break;
						}
					}
				}
			}
		}
	};

	function _jump(dest) {
		let dest_bk;
		if (onJump) {
			while (dest_bk != dest) {
				dest_bk = dest;
				dest = onJump(dest, sess.story.curId) || "";
			}
		}

		sess.story.curId = dest;

		console.log("### jump to", dest);

		if (dest) {
			cm.jump("story", 0, true);
		}
	};


	//======= handle reprompts ========
	function _setReprompts(rep1 /*{script, ssml}*/ , rep2 /*{script, ssml}*/ , repAlexa /*{script, ssml}*/ ) {
		overRepros.REPROMPT1 = rep1;
		overRepros.REPROMPT2 = rep2;
		overRepros.REPROMPT_ALEXA = repAlexa;
	}

	function _getReprompt(reptId) {
		const rep = storyData[reptId];
		if (overRepros[reptId]) {
			rep.script = overRepros[reptId].script;
			rep.ssml = overRepros[reptId].ssml;
		}
		return rep;
	}

	function _sayReprompt(i1or2) {
		_say(_getReprompt("REPROMPT" + i1or2));

		sess.temp.prompt && sb.say(sess.temp.prompt);
	}

	function _getAlexaReprompt() {
		let textOfSsml = "";
		const item = _getReprompt("REPROMPT_ALEXA");
		if (item) {
			let ssml = item.ssml;
			if (ssml) {
				const url = audioUrlBase + ssml.replace(/{ID}/g, "REPROMPT_ALEXA").replace(/.mp3/, data.type == "ALEXA" ? "_alexa.mp3" : ".mp3");
				textOfSsml = `<audio src="${url}"/>`;
			} else {
				textOfSsml = item.script;
			}
		}
		sess.temp.prompt && (textOfSsml += sess.temp.prompt);
		return textOfSsml;
	}
}


module.exports = {
	init: _init,
	loadStory: _loadStory,
	getStory: _getStory,
	StoryManager: _kwStoryManager
};