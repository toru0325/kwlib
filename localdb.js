const config = require(process.env.KW_CONFIG_PATH);
const kw = require("./kw");

class localDB {
    constructor() {
        const _ = kw.prv(this);
        const _this = this;

        const maxCount = config.LOCALDB_MAXCOUNT || 10000;
        const itemLs = [];

        _.init = () => {};

        _.getD = (opt /*{path, default}*/ ) => {
            let item;
            itemLs.some((o, i) => {
                if (o._path == opt.path) {
                    item = o;
                    return true;
                }
            });

            item = item || opt.default;

            item._path = opt.path;

            return kw.Q.resolve(item);
        };

        _.setD = (opt /*{path, value}*/ ) => {
            let item = opt.value;
            itemLs.some((o, i) => {
                if (o._path == opt.path) {
                    itemLs.splice(i, 1);
                    return true;
                }
            });

            if (opt.value !== null) {
                item._path = opt.path;
                itemLs.unshift(item);

                while (itemLs.length > maxCount) {
                    itemLs.pop();
                }
            }

            return kw.Q.resolve();
        };

        _.getOneD = (opt /*{key, equalTo}*/ ) => {
            let item;
            itemLs.some((o, i) => {
                if (o[opt.key] == opt.equalTo) {
                    item = o;
                    return true;
                }
            });
            return kw.Q.resolve(item);
        }

        _.init();
    }
    getD() {
        return kw.prv(this).getD.apply(this, arguments);
    }
    setD() {
        return kw.prv(this).setD.apply(this, arguments);
    }
    getOneD() {
        return kw.prv(this).getOneD.apply(this, arguments);
    }
}

module.exports = new localDB();