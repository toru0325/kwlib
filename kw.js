const Q = require('q');
const request = require('request');
const fs = require('fs');
const df = require('dateformat');

const drive = require('./googledrive');
const db = require("./firestore");


const _getCsvWithCache = async (csvFileId, docCsvPath) => {
    const obj = await db.getD({
        path: docCsvPath
    });
    let csv = obj ? obj.value : null;
    if (!csv) {
        csv = await drive.authAndGetCsvD(csvFileId);
        await db.setD({
            path: docCsvPath,
            value: {
                value: csv
            }
        });
    }
    return csv;
}

const _csvToObj = (csv, onLine) => {
    const data = {};
    let headers;
    let s = csv;
    s = s.replace(/\t/g, "").replace(/""/g, "#%dq%#");
    let m;
    while ((m = s.match(/"([\s\S]*?)"/))) {
        s = s.replace(/"([\s\S]*?)"/, m[1].replace(/,/g, "#%cm%#").replace(/\n/g, "#%n%#"));
    }
    let ls = s.split(/\n/);
    let curId = "";

    console.log("--- csvToObj line count ---", ls.length);
    for (let s of ls) {
        let ls2 = s.split(",").map((s2) => {
            return s2.replace(/#%cm%#/g, ',')
                .replace(/#%dq%#/g, '"')
                .replace(/#%n%#/g, '\n')
                .replace(/\r+$/, "");
        });

        if (!headers) { //=== 最初の行は強制的にヘッダーに
            headers = ls2;

        } else if (!/^[,\s]+$/.test(s)) { //=== メインアイテム [id,name,script...]
            const o = {};
            for (let i = 0; i < headers.length; i++) {
                o[headers[i]] = ls2[i] || "";
            }
            const id = ls2[0];
            if (id) {
                data[curId = id] = o;
            }
            onLine && onLine(o, id, data);
        }
    }
    console.log("--- csvToObj data count ---", Object.keys(data).length);
    // console.log(JSON.stringify(data));

    return data;
};

const _queryToObj = (s, sep = "&", eq = "=") => {
    const obj = {};
    const qs = s.split(sep);
    for (let q of qs) {
        if (q) {
            const [key, val] = q.split(eq);
            obj[key] = val || ""; //=== undefinedだとfirebaseで保存エラーになるため
        }
    }
    return obj;
};

const _escapeHtmlTags = (html) => {
    return html.replace(/&/g, "&amp;").replace(/</g, "&lt;")
        .replace(/>/g, "&gt;").replace(/"/g, "&quot;").replace(/'/g, "&#39;");
}

const _formatDate = (d, fmt /* yyyymmddHHMMss */ ) => {
    return df(d, fmt);
};

const _getRandom = (num) => {
    return Math.floor(Math.random() * num);
};

const _isFileExist = (file) => {
    try {
        fs.statSync(file);
    } catch (err) {
        if (err.code === 'ENOENT') return false
    }
    return true
};

const _setTimeoutD = (msec) => {
    return ((msec) => {
        let d = Q.defer();
        d.timeoutId = setTimeout(d.resolve, msec);
        d.clear = () => {
            clearTimeout(d.timeoutId);
        };
        return d.promise
    })(msec);
}

// { //=== _setTimeoutD test
//     _setTimeoutD(30).then(() => {
//         console.log("test kw.setTimeoutD: OK");
//     }).catch();
// }

const _dig = (rootObj, path) => {
    let obj = rootObj;
    if (path) {
        path = path.replace(/\]/g, "").replace(/\[/g, ".");
        const ls = path.split(".");
        for (let i in ls) {
            if (obj && obj[ls[i]] !== undefined) {
                obj = obj[ls[i]];
            } else {
                obj = null;
                break;
            }
        }
    }
    return obj;
}

// { //=== dig test
//     let a = {
//         a: {
//             b: [{
//                 c: "hoge"
//             }]
//         }
//     };
//     console.log("test kw.dig: ", _dig(a, "a.b[0].c"));
// }


const _prv = (self) => {
    let p;
    const o = _prv.o || (_prv.o = new WeakMap());
    return !(p = o.get(self)) && (p = {}) && !o.set(self, p) || p;
}


const _requestD = (urlOrObj) => {
    return Q.Promise((resolve, reject) => {
        request(urlOrObj, (error, res /*{body, url, method, statusCode, statusMessage, request, ...}*/ , body) => {
            // console.log('_requestD: ', JSON.stringify(res));
            if (!error && res.statusCode == 200) {
                resolve(res);
            } else {
                reject(res);
            }
        });
    });
}

module.exports = {
    getCsvWithCache: _getCsvWithCache,
    csvToObj: _csvToObj,
    queryToObj: _queryToObj,
    escapeHtmlTags: _escapeHtmlTags,
    formatDate: _formatDate,
    getRandom: _getRandom,
    fs: fs,
    isFileExist: _isFileExist,
    Q: Q,
    setTimeoutD: _setTimeoutD,
    dig: _dig,
    prv: _prv,
    requestD: _requestD
}