// const kw = require("./kw");
const ssmlBuilder = require('ssml-builder');

let audioDir = "";
let idWordsTable = {};
let audioUrls = {};

const _init = (cfg /*{audioDir, table:{id:text,...}}, audioUrls:{id:url,...}}}*/ ) => {
	audioDir = cfg.audioDir || "";
	idWordsTable = cfg.table || {};
	audioUrls = cfg.audioUrls || {};
};

const _getSsmlBuilder = (audioSuffix = "") => {
	const sb = new ssmlBuilder();
	let defProsody = {};

	sb._this = sb;
	sb._said = "";
	sb._suffix = audioSuffix;
	sb.say_bk = sb.say;
	sb.pause_bk = sb.pause;
	sb.prosody_bk = sb.prosody;

	sb.setDefaultProsody = cfg =>{
		defProsody = cfg;
	};

	sb.prosodyId = (cfg, id) => {
		return sb.prosody(cfg, idWordsTable[id]);
	};
	sb.sayId = (id) => {
		let o = sb._this;
		let s = idWordsTable[id];
		o._said += s;
		return sb._justSay(s);
	};
	sb.say = (s, displayText) => {
		let o = sb._this;
		o._said += displayText || s;
		return sb._justSay(s);
	};
	sb._justSay = (s) => { //=== 「。」の間が短いので、強制的に0.4秒ポーズにして発話 => Mod190817 間延びする？？
		let o2;
		let s2 = s;
		// const ls = s.replace(/[。！？]+/g, "$&\0。\0").split(/\0/);
		// for (let s2 of ls) {
		// 	if (s2 == "。") {
		// 		o2 = sb.pause("0.4s");
		// 	} else {
				o2 = s2 ? sb.prosody_bk(defProsody, s2) : sb.say_bk(""); //=== prosodyがemptyでエラーになる問題回避
		// 	}
		// }
		return o2;
	};
	sb.sayHidden = (s) => { //=== 発声：有、テキスト：無
		return sb._justSay(s);
	};
	sb.saySilent = (s) => { //=== 発声：無、テキスト：有
		let o = sb._this;
		o._said += s;
	};
	sb.pause = (duration) => {
		sb._validateDuration(duration);
		sb._elements.push("<s><break time='" + duration + "'/></s>");
	};
	sb.getText = () => {
		return sb._this._said.replace(/^--|--$/g, "").replace(/-[\s-]*-/g, "\n\n");
	};
	sb.sayAudio = (id) => {
		let o = sb._this;
		o._said += idWordsTable[id] || "";

		let id2 = id + sb._suffix;
		const url = audioUrls[id2] || (audioDir + id2 + ".mp3")
		return o.audio(url.replace(/&/g, "&amp;"));
	};
	sb.sayAudio2 = (id_or_url /* hoge or http://hoge or http://hoge.mp3 */ , txt) => {
		let o = sb._this;
		let url;
		txt = txt || "";

		if (id_or_url.indexOf("http") == 0) {
			if (id_or_url.match(/http.*\/[^/]+\.mp3$/)) { //=== http://hoge.mp3
				url = id_or_url;
			} else {
				url = id_or_url + sb._suffix + ".mp3"; //=== http://hoge
			}
		} else {
			let id2 = id_or_url + sb._suffix;
			url = audioUrls[id2] || (audioDir + id2 + ".mp3")
		}

		if (url) {
			o._said += txt;
			return o.audio(url.replace(/&/g, "&amp;"));
		} else {
			return o._justSay(txt);
		}
	};
	sb.prosody = (cfg, s, displayText) => {
		let o = sb._this;
		o._said += displayText || s;
		return o.prosody_bk(cfg, s);
	};

	return sb;
};

module.exports = {
	init: _init,
	getSsmlBuilder: _getSsmlBuilder
}