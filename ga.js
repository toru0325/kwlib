const ua = require('universal-analytics');
const uuid_v4 = require('uuid/v4');
const config = require(process.env.KW_CONFIG_PATH);

const track = ({
	uuid,
	url
}) => {
	const visitor = ua(config.GA_ID, uuid, {
		headers: {
			'user-agent': `Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36`
		}
	});
	visitor.pageview(url).send();
};

const newUUID = () => {
	return uuid_v4();
}

module.exports = {
	track,
	newUUID
};