const config = require(process.env.KW_CONFIG_PATH);
const kw = require("../kw");
const Alexa = require('ask-sdk');
const alexaVerifier = require('alexa-verifier');
let webhook;
let skill;


function _setHandlers(app, bodyParser, _webhook) {
    webhook = _webhook;
    // app.get("/alexa_webhook", _onWebhook);

    skill = Alexa.SkillBuilders.custom()
        .addRequestHandlers({
            canHandle: () => true,
            handle: _onHandleIntent
        })
        .create();
    app.post('/alexa_webhook', _onWebhook);
};

function _onWebhook(req, res) {
    //=== 署名をチェック
    alexaVerifier(req.headers.signaturecertchainurl,
        req.headers.signature,
        req.rawBody,
        (err) => {
            if (err) {
                res.status(401).json({
                    message: 'Verification Failure',
                    error: err
                });
            } else {
                skill.invoke(req.body)
                    .then(responseBody => {
                        console.log("===== alaxa.response =====\n", JSON.stringify(responseBody));
                        res.json(responseBody);
                    })
                    .catch(error => {
                        console.log(error);
                        res.status(500).send('Error during the request');
                    });
            }
        }
    );
};

//=== メイン処理
function _onHandleIntent(handlerInput) {
    const body = handlerInput.requestEnvelope;

    console.log("===== alaxa.onWebhook =====\n", JSON.stringify(body));

    if (kw.dig(body, "request.type").match(/SessionEndedRequest|AudioPlayer|PlaybackController/)) {
        console.log("=== request passed! : request = ", body.request.type);
        return handlerInput
            .responseBuilder
            .getResponse();
    }

    // if ((kw.dig(body, "request.intent.name") || "").match(/PauseIntent|ResumeIntent|/)) {
    //     console.log("=== request passed! : intent = ", body.request.intent.name);
    //     return handlerInput
    //         .responseBuilder
    //         .getResponse();
    // }

    const err = kw.dig(body, "request.error");
    if (err) {
        console.log("===== ERROR =====\n", err);
        return handlerInput.responseBuilder
            .speak('何か問題が発生したようです。スキルを終了します。')
            .withShouldEndSession(true)
            .getResponse();
    }

    let text = "";
    const params = {};

    const ls = kw.dig(body, "request.intent.slots");
    ls && Object.keys(ls).forEach((k, i) => {
        params[k] = kw.dig(ls[k], "resolutions.resolutionsPerAuthority[0].values[0].value.id") || ls[k].value; //=== 代表語を採用
        text += params[k] ? params[k] : "";
    });

    let intent = kw.dig(body, "request.intent.name") || "";
    if (kw.dig(body, "session.new") && !intent.match(/AMAZON.PauseIntent|AMAZON.ResumeIntent/)) {
        intent = "welcome";
    } else if (intent.match(/AMAZON.StopIntent|AMAZON.CancelIntent/)) {
        intent = "input_quit";
    } else if (intent == "AMAZON.HelpIntent") {
        intent = "input_help";
    } else if (intent.match(/AMAZON.YesIntent/)) {
        intent = "input_yes";
    } else if (intent.match(/AMAZON.NoIntent/)) {
        intent = "input_no";
    }

    return webhook.onWebhookD({
        type: "ALEXA",
        uid: kw.dig(body, "session.user.userId").replace(/[.#$[\]]/g, "-"), //=== firebaseの禁止文字変換
        sid: kw.dig(body, "session.sessionId").replace(/[.#$[\]]/g, "-"), //=== firebaseの禁止文字変換
        text: text,
        intent: intent,
        parameters: params

    }).then(({
        speech,
        displayText,
        responses /*{simple, media, suggestions, alexa_reprompt, alexa_apl}*/ ,
        isQuit
    }) => {
        const iface = kw.dig(body, "context.System.device.supportedInterfaces"); //=== "AudioPlayer", "Display", "Alexa.Presentation.APL", etc...
        let rb = handlerInput.responseBuilder;

        if ((!speech || speech == "<speak></speak>") && !displayText) {
            speech = displayText = "あれ、何か調子が悪いようです。また後ほどお試しください。"
            isQuit = true;
        }

        //=== スピーチ
        if (speech) {
            rb = rb.speak(speech);
        }

        //=== いろんな方法で画面表示
        let isHandledDisplay = false;

        if (iface["Alexa.Presentation.APL"]) {
            if (responses.alexa_apl) {
                const tmpl = require(responses.alexa_apl.require);
                tmpl.datasources.data = responses.alexa_apl.data || tmpl.datasources.data;
                const token = "token"; // + Date.now(); //=== must use same token with document and execute!
                rb = rb.addDirective({
                    "type": "Alexa.Presentation.APL.RenderDocument",
                    "token": token,
                    "document": tmpl.document,
                    "datasources": tmpl.datasources,
                });

                if (responses.alexa_apl.commands) {
                    rb = rb.addDirective({
                        "type": "Alexa.Presentation.APL.ExecuteCommands",
                        "token": token,
                        "commands": responses.alexa_apl.commands
                    });
                }


                isHandledDisplay = true;
            }
        }

        if (iface.Display) {
            //==== SHOW_FULL_IMAGE 画像をしっかりと見せたい用
            // if (kw.dig(body, "context.System.device.supportedInterfaces.Display")) {
            //     responses.card = {
            //         image: {
            //             url: "https://kw-googlehome.s3-ap-northeast-1.amazonaws.com/captain/images/card8.png",
            //             alt: "六の段"
            //         },
            //         alexa_type: "SHOW_FULL_IMAGE"
            //     };
            // }
            if (kw.dig(responses, "card.alexa_type") == "SHOW_FULL_IMAGE") {
                const img = new Alexa.ImageHelper()
                    .withDescription('image')
                    .addImageInstance(responses.card.image.url.replace(/\.png/, "_alexa.png"))
                    .getImage();
                rb = rb.addRenderTemplateDirective({
                    type: 'BodyTemplate1',
                    backButton: 'HIDDEN',
                    // image: null,
                    backgroundImage: img,
                    // title: null, //responses.card.image.alt,
                    // textContent: new Alexa.RichTextContentHelper()
                    //     .withPrimaryText(`<img src='${responses.card.image.url}' width='380' height='300' alt='image' />`)
                    //     .getTextContent(),
                    token: "IMAGE_" + Date.now(),
                });
                isHandledDisplay = true;
            }
        }

        if (!isHandledDisplay) {
            rb = rb.withStandardCard(
                config.app_title,
                displayText,
                null,
                kw.dig(responses, "card.image.url") || null
            );
        }

        if (iface.AudioPlayer) {
            if (responses.media) {
                // サンプルデータ： sess.temp.responses.media = {
                //     name: 'おやすみさんトーク',
                //     url: 'https://d30461f9.ngrok.io/sounds/main.mp3',
                //     description: '今日の日替わりトークです。',
                //     icon: {
                //         url: 'data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMwLjA1MSAzMC4wNTEiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMwLjA1MSAzMC4wNTE7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNjRweCIgaGVpZ2h0PSI2NHB4Ij4KPGc+Cgk8cGF0aCBkPSJNMTkuOTgyLDE0LjQzOGwtNi4yNC00LjUzNmMtMC4yMjktMC4xNjYtMC41MzMtMC4xOTEtMC43ODQtMC4wNjJjLTAuMjUzLDAuMTI4LTAuNDExLDAuMzg4LTAuNDExLDAuNjY5djkuMDY5ICAgYzAsMC4yODQsMC4xNTgsMC41NDMsMC40MTEsMC42NzFjMC4xMDcsMC4wNTQsMC4yMjQsMC4wODEsMC4zNDIsMC4wODFjMC4xNTQsMCwwLjMxLTAuMDQ5LDAuNDQyLTAuMTQ2bDYuMjQtNC41MzIgICBjMC4xOTctMC4xNDUsMC4zMTItMC4zNjksMC4zMTItMC42MDdDMjAuMjk1LDE0LjgwMywyMC4xNzcsMTQuNTgsMTkuOTgyLDE0LjQzOHoiIGZpbGw9IiMwMDAwMDAiLz4KCTxwYXRoIGQ9Ik0xNS4wMjYsMC4wMDJDNi43MjYsMC4wMDIsMCw2LjcyOCwwLDE1LjAyOGMwLDguMjk3LDYuNzI2LDE1LjAyMSwxNS4wMjYsMTUuMDIxYzguMjk4LDAsMTUuMDI1LTYuNzI1LDE1LjAyNS0xNS4wMjEgICBDMzAuMDUyLDYuNzI4LDIzLjMyNCwwLjAwMiwxNS4wMjYsMC4wMDJ6IE0xNS4wMjYsMjcuNTQyYy02LjkxMiwwLTEyLjUxNi01LjYwMS0xMi41MTYtMTIuNTE0YzAtNi45MSw1LjYwNC0xMi41MTgsMTIuNTE2LTEyLjUxOCAgIGM2LjkxMSwwLDEyLjUxNCw1LjYwNywxMi41MTQsMTIuNTE4QzI3LjU0MSwyMS45NDEsMjEuOTM3LDI3LjU0MiwxNS4wMjYsMjcuNTQyeiIgZmlsbD0iIzAwMDAwMCIvPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=',
                //         alt: 'Icon',
                //     }
                // };
                rb.addAudioPlayerPlayDirective('REPLACE_ALL', responses.media.url, "MUSIC_" + Date.now(), 0, "", {
                    title: responses.media.name,
                    subtitle: responses.media.description,
                    art: {
                        sources: [{
                            url: responses.media.icon.url
                        }]
                    }
                });
            }

            if (isQuit && !responses.media) {
                rb.addAudioPlayerStopDirective();
            }
        }

        //=== reprompt
        rb = rb.reprompt(responses.alexa_reprompt || "すみません、聞き取れませんでした。もう一度お願いします。");

        //=== セッション終了かどうか
        isQuit && (rb.withShouldEndSession(true));

        return rb.getResponse();

    }).catch((err) => {
        console.log("\n%%% Error!! on alexa.main.onWebhookD.catch\n", err);
    });
}


module.exports = {
    setHandlers: _setHandlers
};