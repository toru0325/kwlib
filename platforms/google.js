const kw = require("../kw");
const uuid = require('uuid/v4');
const {
    dialogflow,
    BasicCard,
    BrowseCarousel,
    BrowseCarouselItem,
    Button,
    // Carousel,
    Image,
    // LinkOutSuggestion,
    // List,
    // MediaResponse,
    MediaObject,
    Suggestions,
    SimpleResponse,
    // Table,
} = require('actions-on-google');

let webhook;
const dfApp = dialogflow();

function _setHandlers(app, bodyParser, _webhook) {
    webhook = _webhook;

    dfApp.fallback(_onWebhook);
    app.get("/google_webhook", _onWebhook);
    app.use("/google_webhook", bodyParser.json(), dfApp);
};

function _onWebhook(conv) {
    console.log("===== google.webhook =====\n" + JSON.stringify(conv));

    if (kw.dig(conv, "user.name.family") == "Crawler") {
        console.log("### Crawler is commig!");
        conv.close("OK");
        return;
    }

    if (kw.dig(conv, "query") == "actions_intent_MEDIA_STATUS") { //=== may be caused on Google Home only.
        console.log("### MEDIA_STATUS is happend!");
        conv.close(".");
        return;
    }

    let uid = conv.user.storage.userId;
    let old_uid = null;
    if (!uid) {
        old_uid = kw.dig(conv, "user.id");
        uid = "google-" + (old_uid || uuid());
        conv.user.storage.userId = uid;
    }
    let sid = kw.dig(conv, "body.originalDetectIntentRequest.payload.conversation.conversationId");

    let text = "";
    let params = conv.parameters;
    Object.keys(params).forEach((k, i) => {
        text += params[k] ? params[k] : "";
    });
    // text = text || kw.dig(conv, "body.queryResult.queryText"); //=== Mod190530 インテントベースで使うからslotがないものは拾わない

    let intent = conv.intent;
    if (kw.dig(conv, "request.conversation.type") == "NEW") {
        intent = "welcome";
    }

    return webhook.onWebhookD({
        type: "GOOGLE",
        uid: uid.replace(/\//g, "_"), //=== スラッシュはdb保存時に影響あるので
        sid: sid.replace(/\//g, "_"), //=== スラッシュはdb保存時に影響あるので
        old_uid: old_uid, //=== uid以降時に、全uidを参照できるように保存
        text: text,
        intent: intent,
        parameters: params
    }).then(({
        speech,
        displayText,
        responses,
        /*{simple, media, suggestions}*/
        isQuit
    }) => {
        //=== 無言を回避
        if ((!speech || speech == "<speak></speak>") && !displayText) {
            speech = displayText = "あれ、何か調子が悪いようです。また後ほどお試しください。"
            isQuit = true;
        }

        let askOrClose = isQuit ? "close" : "ask";

        //=== メディア再生でcloseできないので、media_statusへ処理をまわす。（要インテント！）
        if (askOrClose == "close" && responses.media && !conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
            askOrClose = "ask";
            console.log("### Media Response with 'close' on google home detected!! Forth change to 'ask'.");
        }

        conv[askOrClose](new SimpleResponse({
            speech: speech,
            text: displayText
        }));

        if (responses.card) {
            conv[askOrClose](new BasicCard({
                text: responses.card.text,
                subtitle: responses.card.subtitle,
                title: responses.card.title,
                buttons: responses.card.buttons ? new Button(responses.card.buttons) : null,
                image: responses.card.image ? new Image(responses.card.image) : null,
            }));
        }

        if (responses.media) {
            // サンプルデータ： sess.temp.responses.media = {
            //     name: 'おやすみさんトーク',
            //     url: 'https://d30461f9.ngrok.io/sounds/main.mp3',
            //     description: '今日の日替わりトークです。',
            //     icon: {
            //         url: 'data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTguMS4xLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMwLjA1MSAzMC4wNTEiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMwLjA1MSAzMC4wNTE7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNjRweCIgaGVpZ2h0PSI2NHB4Ij4KPGc+Cgk8cGF0aCBkPSJNMTkuOTgyLDE0LjQzOGwtNi4yNC00LjUzNmMtMC4yMjktMC4xNjYtMC41MzMtMC4xOTEtMC43ODQtMC4wNjJjLTAuMjUzLDAuMTI4LTAuNDExLDAuMzg4LTAuNDExLDAuNjY5djkuMDY5ICAgYzAsMC4yODQsMC4xNTgsMC41NDMsMC40MTEsMC42NzFjMC4xMDcsMC4wNTQsMC4yMjQsMC4wODEsMC4zNDIsMC4wODFjMC4xNTQsMCwwLjMxLTAuMDQ5LDAuNDQyLTAuMTQ2bDYuMjQtNC41MzIgICBjMC4xOTctMC4xNDUsMC4zMTItMC4zNjksMC4zMTItMC42MDdDMjAuMjk1LDE0LjgwMywyMC4xNzcsMTQuNTgsMTkuOTgyLDE0LjQzOHoiIGZpbGw9IiMwMDAwMDAiLz4KCTxwYXRoIGQ9Ik0xNS4wMjYsMC4wMDJDNi43MjYsMC4wMDIsMCw2LjcyOCwwLDE1LjAyOGMwLDguMjk3LDYuNzI2LDE1LjAyMSwxNS4wMjYsMTUuMDIxYzguMjk4LDAsMTUuMDI1LTYuNzI1LDE1LjAyNS0xNS4wMjEgICBDMzAuMDUyLDYuNzI4LDIzLjMyNCwwLjAwMiwxNS4wMjYsMC4wMDJ6IE0xNS4wMjYsMjcuNTQyYy02LjkxMiwwLTEyLjUxNi01LjYwMS0xMi41MTYtMTIuNTE0YzAtNi45MSw1LjYwNC0xMi41MTgsMTIuNTE2LTEyLjUxOCAgIGM2LjkxMSwwLDEyLjUxNCw1LjYwNywxMi41MTQsMTIuNTE4QzI3LjU0MSwyMS45NDEsMjEuOTM3LDI3LjU0MiwxNS4wMjYsMjcuNTQyeiIgZmlsbD0iIzAwMDAwMCIvPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgoJPGc+Cgk8L2c+Cgk8Zz4KCTwvZz4KCTxnPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=',
            //         alt: 'Icon',
            //     }
            // };
            if (conv.surface.capabilities.has('actions.capability.AUDIO_OUTPUT')) {
                responses.media && conv[askOrClose](new MediaObject(responses.media));
            } else {
                conv[askOrClose]("（このデバイスはオーディオ再生に対応していません）");
            }
        }

        if (conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT')) {
            if (responses.browseCarousel) {
                const items = [];
                for (const json of responses.browseCarousel.items) {
                    let item = JSON.parse(JSON.stringify(json));
                    item.image = new Image(item.image);
                    items.push(new BrowseCarouselItem(item));
                }
                conv[askOrClose](new BrowseCarousel({
                    items: items
                }));
            }

            if (responses.linkOutSuggestion) {
                conv[askOrClose](new LinkOutSuggestion(responses.linkOutSuggestion));
            }

            if (askOrClose != "close") {
                responses.suggestions && conv[askOrClose](new Suggestions(responses.suggestions));
            } else {
                conv[askOrClose]();
            }
        }

    }).catch((err) => {
        console.log("\n%%% Error!! on google.main.onWebhookD.catch\n", err);
    });
}



module.exports = {
    setHandlers: _setHandlers
};



//==== sample code

// Copyright 2017-2018, Google, Inc.
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// 'use strict';

// const {
//   dialogflow,
//   BasicCard,
//   BrowseCarousel,
//   BrowseCarouselItem,
//   Button,
//   Carousel,
//   Image,
//   LinkOutSuggestion,
//   List,
//   MediaObject,
//   Suggestions,
//   SimpleResponse,
//   Table,
//  } = require('actions-on-google');
// const functions = require('firebase-functions');

// // Constants for list and carousel selection
// const SELECTION_KEY_GOOGLE_ALLO = 'googleAllo';
// const SELECTION_KEY_GOOGLE_HOME = 'googleHome';
// const SELECTION_KEY_GOOGLE_PIXEL = 'googlePixel';
// const SELECTION_KEY_ONE = 'title';

// // Constant for image URLs
// const IMG_URL_AOG = 'https://developers.google.com/actions/images/badges' +
//   '/XPM_BADGING_GoogleAssistant_VER.png';
// const IMG_URL_GOOGLE_ALLO = 'https://allo.google.com/images/allo-logo.png';
// const IMG_URL_GOOGLE_HOME = 'https://lh3.googleusercontent.com' +
//   '/Nu3a6F80WfixUqf_ec_vgXy_c0-0r4VLJRXjVFF_X_CIilEu8B9fT35qyTEj_PEsKw';
// const IMG_URL_GOOGLE_PIXEL = 'https://storage.googleapis.com/madebygoog/v1' +
//   '/Pixel/Pixel_ColorPicker/Pixel_Device_Angled_Black-720w.png';
// const IMG_URL_MEDIA = 'http://storage.googleapis.com/automotive-media/album_art.jpg';
// const MEDIA_SOURCE = 'http://storage.googleapis.com/automotive-media/Jazz_In_Paris.mp3';

// // Constants for selected item responses
// const SELECTED_ITEM_RESPONSES = {
//   [SELECTION_KEY_ONE]: 'You selected the first item in the list or carousel',
//   [SELECTION_KEY_GOOGLE_HOME]: 'You selected the Google Home!',
//   [SELECTION_KEY_GOOGLE_PIXEL]: 'You selected the Google Home!',
//   [SELECTION_KEY_GOOGLE_PIXEL]: 'You selected the Google Pixel!',
//   [SELECTION_KEY_GOOGLE_ALLO]: 'You selected Google Allo!',
// };

// const intentSuggestions = [
//   'Basic Card',
//   'Browse Carousel',
//   'Carousel',
//   'List',
//   'Media',
//   'Suggestions',
//   'Table',
// ];

// const app = dialogflow({debug: true});

// app.middleware((conv) => {
//   conv.hasScreen =
//     conv.surface.capabilities.has('actions.capability.SCREEN_OUTPUT');
//   conv.hasAudioPlayback =
//     conv.surface.capabilities.has('actions.capability.AUDIO_OUTPUT');
// });

// // Welcome
// app.intent('Default Welcome Intent', (conv) => {
//   conv.ask(new SimpleResponse({
//     speech: 'Hi there!',
//     text: 'Hello there!',
//   }));
//   conv.ask(new SimpleResponse({
//     speech: 'I can show you basic cards, lists and carousels ' +
//       'as well as suggestions on your phone.',
//     text: 'I can show you basic cards, lists and carousels as ' +
//       'well as suggestions.',
//   }));
//   conv.ask(new Suggestions(intentSuggestions));
// });

// app.intent('normal ask', (conv) => {
//   conv.ask('Ask me to show you a list, carousel, or basic card.');
// });

// // Suggestions
// app.intent('suggestions', (conv) => {
//   if (!conv.hasScreen) {
//     conv.ask('Sorry, try this on a screen device or select the ' +
//       'phone surface in the simulator.');
//     return;
//   }
//   conv.ask('This is a simple response for suggestions.');
//   conv.ask(new Suggestions('Suggestion Chips'));
//   conv.ask(new Suggestions(intentSuggestions));
//   conv.ask(new LinkOutSuggestion({
//     name: 'Suggestion Link',
//     url: 'https://assistant.google.com/',
//   }));
// });

// // Basic card
// app.intent('basic card', (conv) => {
//   if (!conv.hasScreen) {
//     conv.ask('Sorry, try this on a screen device or select the ' +
//       'phone surface in the simulator.');
//     return;
//   }
//   conv.ask('This is the first simple response for a basic card.');
//   conv.ask(new Suggestions(intentSuggestions));
//   // Create a basic card
//   conv.ask(new BasicCard({
//     text: `This is a basic card.  Text in a basic card can include "quotes" and
//     most other unicode characters including emoji 📱.  Basic cards also support
//     some markdown formatting like *emphasis* or _italics_, **strong** or
//     __bold__, and ***bold itallic*** or ___strong emphasis___ as well as other
//     things like line  \nbreaks`, // Note the two spaces before '\n' required for
//                                  // a line break to be rendered in the card.
//     subtitle: 'This is a subtitle',
//     title: 'Title: this is a title',
//     buttons: new Button({
//       title: 'This is a button',
//       url: 'https://assistant.google.com/',
//     }),
//     image: new Image({
//       url: IMG_URL_AOG,
//       alt: 'Image alternate text',
//     }),
//   }));
//   conv.ask(new SimpleResponse({
//     speech: 'This is the second simple response.',
//     text: 'This is the 2nd simple response.',
//   }));
// });

// // List
// app.intent('list', (conv) => {
//   if (!conv.hasScreen) {
//     conv.ask('Sorry, try this on a screen device or select the ' +
//       'phone surface in the simulator.');
//     return;
//   }
//   conv.ask('This is a simple response for a list.');
//   conv.ask(new Suggestions(intentSuggestions));
//   // Create a list
//   conv.ask(new List({
//     title: 'List Title',
//     items: {
//       // Add the first item to the list
//       [SELECTION_KEY_ONE]: {
//         synonyms: [
//           'synonym of title 1',
//           'synonym of title 2',
//           'synonym of title 3',
//         ],
//         title: 'Title of First List Item',
//         description: 'This is a description of a list item.',
//         image: new Image({
//           url: IMG_URL_AOG,
//           alt: 'Image alternate text',
//         }),
//       },
//       // Add the second item to the list
//       [SELECTION_KEY_GOOGLE_HOME]: {
//         synonyms: [
//           'Google Home Assistant',
//           'Assistant on the Google Home',
//       ],
//         title: 'Google Home',
//         description: 'Google Home is a voice-activated speaker powered by ' +
//           'the Google Assistant.',
//         image: new Image({
//           url: IMG_URL_GOOGLE_HOME,
//           alt: 'Google Home',
//         }),
//       },
//       // Add the third item to the list
//       [SELECTION_KEY_GOOGLE_PIXEL]: {
//         synonyms: [
//           'Google Pixel XL',
//           'Pixel',
//           'Pixel XL',
//         ],
//         title: 'Google Pixel',
//         description: 'Pixel. Phone by Google.',
//         image: new Image({
//           url: IMG_URL_GOOGLE_PIXEL,
//           alt: 'Google Pixel',
//         }),
//       },
//       // Add the last item to the list
//       [SELECTION_KEY_GOOGLE_ALLO]: {
//         title: 'Google Allo',
//         synonyms: [
//           'Allo',
//         ],
//         description: 'Introducing Google Allo, a smart messaging app that ' +
//           'helps you say more and do more.',
//         image: new Image({
//           url: IMG_URL_GOOGLE_ALLO,
//           alt: 'Google Allo Logo',
//         }),
//       },
//     },
//   }));
// });

// // Carousel
// app.intent('carousel', (conv) => {
//   if (!conv.hasScreen) {
//     conv.ask('Sorry, try this on a screen device or select the ' +
//       'phone surface in the simulator.');
//     return;
//   }
//   conv.ask('This is a simple response for a carousel.');
//   conv.ask(new Suggestions(intentSuggestions));
//   // Create a carousel
//   conv.ask(new Carousel({
//     items: {
//       // Add the first item to the carousel
//       [SELECTION_KEY_ONE]: {
//         synonyms: [
//           'synonym of title 1',
//           'synonym of title 2',
//           'synonym of title 3',
//         ],
//         title: 'Title of First Carousel Item',
//         description: 'This is a description of a carousel item.',
//         image: new Image({
//           url: IMG_URL_AOG,
//           alt: 'Image alternate text',
//         }),
//       },
//       // Add the second item to the carousel
//       [SELECTION_KEY_GOOGLE_HOME]: {
//         synonyms: [
//           'Google Home Assistant',
//           'Assistant on the Google Home',
//       ],
//         title: 'Google Home',
//         description: 'Google Home is a voice-activated speaker powered by ' +
//           'the Google Assistant.',
//         image: new Image({
//           url: IMG_URL_GOOGLE_HOME,
//           alt: 'Google Home',
//         }),
//       },
//       // Add third item to the carousel
//       [SELECTION_KEY_GOOGLE_PIXEL]: {
//         synonyms: [
//           'Google Pixel XL',
//           'Pixel',
//           'Pixel XL',
//         ],
//         title: 'Google Pixel',
//         description: 'Pixel. Phone by Google.',
//         image: new Image({
//           url: IMG_URL_GOOGLE_PIXEL,
//           alt: 'Google Pixel',
//         }),
//       },
//       // Add last item of the carousel
//       [SELECTION_KEY_GOOGLE_ALLO]: {
//         title: 'Google Allo',
//         synonyms: [
//           'Allo',
//         ],
//         description: 'Introducing Google Allo, a smart messaging app that ' +
//           'helps you say more and do more.',
//         image: new Image({
//           url: IMG_URL_GOOGLE_ALLO,
//           alt: 'Google Allo Logo',
//         }),
//       },
//     },
//   }));
// });

// // Browse Carousel
// app.intent('browse carousel', (conv) => {
//   const a11yText = 'Google Assistant Bubbles';
//   const googleUrl = 'https://google.com';
//   if (!conv.hasScreen) {
//     conv.ask('Sorry, try this on a screen device or select the ' +
//       'phone surface in the simulator.');
//     return;
//   }
//   conv.ask('This is an example of a "Browse Carousel"');
//   // Create a browse carousel
//   conv.ask(new BrowseCarousel({
//     items: [
//       new BrowseCarouselItem({
//         title: 'Title of item 1',
//         url: googleUrl,
//         description: 'Description of item 1',
//         image: new Image({
//           url: IMG_URL_AOG,
//           alt: a11yText,
//         }),
//         footer: 'Item 1 footer',
//       }),
//       new BrowseCarouselItem({
//         title: 'Title of item 2',
//         url: googleUrl,
//         description: 'Description of item 2',
//         image: new Image({
//           url: IMG_URL_AOG,
//           alt: a11yText,
//         }),
//         footer: 'Item 2 footer',
//       }),
//     ],
//   }));
// });

// // Media response
// app.intent('media response', (conv) => {
//   if (!conv.hasAudioPlayback) {
//     conv.ask('Sorry, this device does not support audio playback.');
//     return;
//   }
//   conv.ask('This is the first simple response for a media response');
//   conv.ask(new MediaObject({
//     name: 'Jazz in Paris',
//     url: MEDIA_SOURCE,
//     description: 'A funky Jazz tune',
//     icon: new Image({
//       url: IMG_URL_MEDIA,
//       alt: 'Media icon',
//     }),
//   }));
//   conv.ask(new Suggestions(intentSuggestions));
// });

// // Handle a media status event
// app.intent('media status', (conv) => {
//   const mediaStatus = conv.arguments.get('MEDIA_STATUS');
//   let response = 'Unknown media status received.';
//   if (mediaStatus && mediaStatus.status === 'FINISHED') {
//     response = 'Hope you enjoyed the tunes!';
//   }
//   conv.ask(response);
//   conv.ask(new Suggestions(intentSuggestions));
// });

// // React to list or carousel selection
// app.intent('item selected', (conv, params, option) => {
//   let response = 'You did not select any item from the list or carousel';
//   if (option && SELECTED_ITEM_RESPONSES.hasOwnProperty(option)) {
//     response = SELECTED_ITEM_RESPONSES[option];
//   } else {
//     response = 'You selected an unknown item from the list or carousel';
//   }
//   conv.ask(response);
// });

// app.intent('card builder', (conv) => {
//   if (!conv.hasScreen) {
//     conv.ask('Sorry, try this on a screen device or select the ' +
//       'phone surface in the simulator.');
//     return;
//   }
//   conv.ask(...conv.incoming);
//   conv.ask(new BasicCard({
//     text: `Actions on Google let you build for
//     the Google Assistant. Reach users right when they need you. Users don’t
//     need to pre-enable skills or install new apps.  \n  \nThis was written
//     in the fulfillment webhook!`,
//     subtitle: 'Engage users through the Google Assistant',
//     title: 'Actions on Google',
//     buttons: new Button({
//       title: 'Developer Site',
//       url: 'https://developers.google.com/actions/',
//     }),
//     image: new Image({
//       url: IMG_URL_AOG,
//       alt: 'Actions on Google',
//     }),
//   }));
// });

// app.intent('table builder', (conv) => {
//   if (!conv.hasScreen) {
//     conv.ask('Sorry, try this on a screen device or select the ' +
//       'phone surface in the simulator.');
//     return;
//   }
//   conv.ask('You can include table data like this')
//   conv.ask(new Table({
//     dividers: true,
//     columns: ['header 1', 'header 2', 'header 3'],
//     rows: [
//       ['row 1 item 1', 'row 1 item 2', 'row 1 item 3'],
//       ['row 2 item 1', 'row 2 item 2', 'row 2 item 3'],
//     ],
//   }));
// });

// // Leave conversation with card
// app.intent('bye card', (conv) => {
//   if (!hasScreen) {
//     conv.ask('Sorry, try this on a screen device or select the phone ' +
//       'surface in the simulator.');
//     return;
//   }
//   conv.ask('Goodbye, World!');
//   conv.close(new BasicCard({
//     text: 'This is a goodbye card.',
//   }));
// });

// // Leave conversation with SimpleResponse
// app.intent('bye response', (conv) => {
//   conv.close(new SimpleResponse({
//     speech: 'Okay see you later',
//     text: 'OK see you later!',
//   }));
// });

// // Leave conversation
// app.intent('normal bye', (conv) => {
//   conv.close('Okay see you later!');
// });

// exports.conversationComponent = functions.https.onRequest(app);