'use strict';

const config = {
  channelAccessToken: "fFDD7BnkMVjOJyoKAimAu6yuwx8zaHglrJzxXxBdlDNjIcoa35KL8LUkZu3nffFSSE9DD1EZKcwpHDTIsTejgDMn/1qPakj5V8MwsHBrRLHNh6T/Csw0PZncTw5x6PA0lE4XbXROLFwnUel/d5gFxAdB04t89/1O/w1cDnyilFU=",
  channelSecret: "52a4d44d5b0551667996fd15c9bd77cf",
  // "liffId":"1606394727-5WzaxGrK"
};



// event handler
const _getMessage = (event) => {
  if (event.type !== 'message' || event.message.type !== 'text') {
    return null;
  }

  let msg = "";
  //   if (event.message.text.match(/デモ|demo/)) {
  //     msg = `\u{10003D}デモ用の地図です↓
  // line://app/1606394727-5WzaxGrK/?demo`;
  //   } else 
  if (event.message.text.match(/起動|スタート/)) {
    msg = "\u{100077}起動するには、Clovaに向かって「ゾンビのまちを開いて」と言ってください。";

  } else {
    msg = `\u{10003D}ケンタくんから地図が届いています↓
line://app/1606394727-5WzaxGrK

\u{100077}マップは自動で更新され、アイテムやメモ、イベントなど、つねに最新情報を表示します。`
  }

  return {
    type: 'text',
    text: msg
  };
}


const _lineBot = (app) => {
  const line = require('@line/bot-sdk');
  const client = new line.Client(config);

  // register a webhook handler with middleware
  // about the middleware, please refer to doc
  app.post('/linebot_webhook', line.middleware(config), (req, res) => {
    console.log("===== linebot.onWebhook =====\n", JSON.stringify(req.body));

    Promise
      .all(req.body.events.map((event) => {
        let msg = _getMessage(event);
        return msg ? client.replyMessage(event.replyToken, msg) : Promise.resolve(null);
      }))
      .then((result) => res.json(result))
      .catch((err) => {
        console.error(err);
        res.status(500).end();
      });
  });
}

module.exports = _lineBot;