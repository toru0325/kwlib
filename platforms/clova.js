const kw = require("../kw");
const cheerio = require('cheerio');
let webhook;

function _setHandlers(app, bodyParser, _webhook) {
    webhook = _webhook;
    // app.get("/clova_webhook", _onWebhook);
    app.post("/clova_webhook", _onWebhook);
};

//=== 署名をチェック
function _onWebhook(req, res) {
    try {
        _onWebhook_main(req, res);
    } catch (err) {
        _sendError(err);
    }
};

//=== メイン処理
function _onWebhook_main(req, res) {
    console.log("===== clova.onWebhook =====\n", JSON.stringify(req.body));

    if (!kw.dig(req.body, "session")) {
        console.log("### Session not found! Bot?");
        _send(res, _createClovaJson({
            simpleSpeech: "不明なセッションです。",
            shouldEndSession: true
        }));
        return;
    }

    //=== for browser test (GET)
    if (req.method == "GET") {
        res.send("OK!");
        return;
    }

    //=== bodyがないエラー
    if (!req.body) {
        let err = kw.dig(req.body, "request.error");
        err = err ? JSON.stringify(err) : "Body has no request!";
        throw (err);
    }

    //=== セッション閉じた後のイベント処理
    if (kw.dig(req, "body.request.type") == "SessionEndedRequest") {
        console.log("=== session ended request");
        _send(res, {});
        return;
    }

    let text = "";
    const params = {};

    //=== スロットの中身を収集
    let ls = kw.dig(req.body, "request.intent.slots");
    ls && Object.keys(ls).forEach((k, i) => {
        params[k] = ls[k].value;
        text += params[k] ? params[k] : "";
    });

    //=== オフィシャルインテントを発話にマッピング
    let intent = kw.dig(req.body, "request.intent.name") || "";
    if (kw.dig(req.body, "request.type") == "LaunchRequest") {
        intent = "welcome";
    } else if (intent.match(/Clova.CancelIntent/)) {
        intent = "input_quit";
    } else if (intent.match(/Clova.GuideIntent/)) {
        intent = "input_help";
    } else if (intent.match(/Clova.YesIntent/)) {
        intent = "input_yes";
    } else if (intent.match(/Clova.NoIntent/)) {
        intent = "input_no";
    }

    let uid = "clova-" + kw.dig(req.body, "session.user.userId");
    // if (uid == "clova-U326d8bd598b6c0eac5feee75495eaa85"){
    //     uid = "clova-demo20181110"; //=== LINE BOOT AWARDS用
    // }

    webhook.onWebhookD({
        type: "CLOVA",
        uid: uid.replace(/[.#$[\]]/g, "-"), //=== firebaseの禁止文字変換
        sid: kw.dig(req.body, "session.sessionId").replace(/[.#$[\]]/g, "-"), //=== firebaseの禁止文字変換
        text: text,
        intent: intent,
        parameters: params

    }).then(({
        speech,
        displayText,
        responses /*{simple, media, suggestions, alexa_reprompt}*/ ,
        isQuit
    }) => {
        let speechValues;
        if (speech.match(/^<speak>/)) {
            speechValues = _ssmlToSpeechValues(speech);
        } else {
            speechValues = [{
                "type": "PlainText",
                "lang": "ja",
                "value": speech
            }];
        }

        if (responses.media) {
            speechValues.push({
                "type": "URL",
                "value": responses.media.url
            });
        }

        _send(res, _createClovaJson({
            speechValues,
            reprompt: responses.alexa_reprompt || "すみません、聞き取れませんでした。もう一度お願いします。",
            shouldEndSession: isQuit
        }));

    }).then(null, _sendError).catch((err) => {
        console.log("\n%%% Error!! on clova.main.onWebhookD.catch\n", err);
    });
};

function _createClovaJson({
    speechValues,
    simpleSpeech,
    reprompt,
    shouldEndSession
}) {
    return {
        "version": "1.0",
        "sessionAttributes": {},
        "response": {
            "outputSpeech": speechValues ? {
                "type": (speechValues instanceof Array) ? "SpeechList" : "SimpleSpeech",
                "values": speechValues
            } : simpleSpeech ? {
                "type": "SimpleSpeech",
                "values": {
                    "type": "PlainText",
                    "lang": "ja",
                    "value": simpleSpeech
                }
            } : null,
            "reprompt": {
                "outputSpeech": {
                    "type": "SimpleSpeech",
                    "values": {
                        "type": "PlainText",
                        "lang": "ja",
                        "value": reprompt || "すみません、聞き取れませんでした。もう一度お願いします。"
                    }
                }
            },
            "directives": [],
            "shouldEndSession": shouldEndSession || false
        }
    }
};

function _ssmlToSpeechValues(ssml) {
    const values = [];
    const $ = cheerio.load(ssml, {
        xmlMode: true,
    });
    let j = $('speak').contents();
    j.each((i, elem) => {
        if (elem.name == "audio") {
            values.push({
                "type": "URL",
                "value": $(elem).attr("src")
            });
        } else if (elem.name == "break") {
            //=== nothing
        } else {
            let s = $(elem).text().replace(/\s/g, "");
            s && values.push({
                "type": "PlainText",
                "lang": "ja",
                "value": s
            });
        }
    });
    return values;
};

function _send(res, resultObj) {
    console.log("===== clova.response =====\n", JSON.stringify(resultObj));

    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(resultObj));
};

function _sendError(err) {
    console.log("===== ERROR =====\n", err);
    _send(res, _createClovaJson({
        simpleSpeech: "申し訳ありません。エラーが発生しています。",
        shouldEndSession: true
    }));
};


module.exports = {
    setHandlers: _setHandlers
};



//=== request sample
//  {
// 	"version": "1.0",
// 	"session": {
// 		"new": false,
// 		"sessionAttributes": {},
// 		"sessionId": "2138ef20-3b0e-4ea4-b25c-09d59e85c6b4",
// 		"user": {
// 			"userId": "U326d8bd598b6c0eac5feee75495eaa85"
// 		}
// 	},
// 	"context": {
// 		"System": {
// 			"application": {
// 				"applicationId": "com.kantetsu.test"
// 			},
// 			"device": {
// 				"deviceId": "f7c0d9c641f44ff28c4663d09f438efffb92f8d8c3875933d8f65b278ac2e94a",
// 				"display": {
// 					"size": "none",
// 					"contentLayer": {
// 						"width": 0,
// 						"height": 0
// 					}
// 				}
// 			},
// 			"user": {
// 				"userId": "U326d8bd598b6c0eac5feee75495eaa85"
// 			}
// 		}
// 	},
// 	"request": {
// 		"type": "IntentRequest", //=== LaunchRequest, IntentRequest, SessionEndedRequest
// 		"requestId": "faf2d212-f38f-4874-828b-5c9844a85549",
// 		"timestamp": "2018-06-01T16:23:16Z",
// 		"locale": "ja-JP",
// 		"extensionId": "com.kantetsu.test",
// 		"intent": {
// 			"intent": "testIntent",
// 			"name": "testIntent",
// 			"slots": {
// 				"test": {
// 					"name": "test",
// 					"value": "こんにちは"
// 				}
// 			}
// 		},
// 		"event": {
// 			"namespace": "",
// 			"name": "",
// 			"payload": null
// 		}
// 	}
// }


//=== response sample
// {
//     "version": "0.1.0",
//     "sessionAttributes": {},
//     "response": {
//         "outputSpeech": {
//             "type": "SimpleSpeech",
//             "values": {
//                 "type": "PlainText",
//                 "lang": "ja",
//                 "value": "こんにちは。"
//             }
//         },
//         "reprompt": {
//             "outputSpeech": {
//                 "type": "SimpleSpeech",
//                 "values": {
//                     "type": "PlainText",
//                     "lang": "ja",
//                     "value": "すみません、聞き取れませんでした。もう一度お願いします。"
//                 }
//             }
//         },
//         "card": {},
//         "directives": [],
//         "shouldEndSession": false
//     }
// };