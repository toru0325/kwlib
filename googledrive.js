const config = require(process.env.KW_CONFIG_PATH);

/*
*
* Google Drive にある Spread Sheet をCSV形式で取得するモジュール
*
*/

//=== https://developers.google.com/drive/api/v3/quickstart/nodejs
const CREDENTIALS = config.GOOGLEDRIVE_CREDENTIALS;

//=== トークンをリセットする場合は下記のファイルを削除
const TOKEN = config.GOOGLEDRIVE_TOKEN;

//=== promiseのエラーをモニタできるように
process.on('unhandledRejection', console.dir);

/*
 *
 *
 *
 */

function GoogleDriveCsv() {
	const readline = require('readline');
	const {
		google
	} = require('googleapis');
	let oAuth2Client;

	// If modifying these scopes, delete token.json.
	const SCOPES = ['https://www.googleapis.com/auth/drive.readonly'];
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.

	this.authAndGetCsvD = _authAndGetCsvD;
	this.getCsvD = _getCsvD;
	//
	this.test = _test;

	// _test();

	async function _authAndGetCsvD(fileId) {
		try {
			if (_isAuthorizeOK(CREDENTIALS)) {
				return await _getCsvD(fileId);
			} else {
				if (await _getAccessTokenD()) {
					return await _getCsvD(fileId);
				}
			}
		} catch (e) {
			console.log("### _authAndGetCsvD error:", e);
		}
	};

	function _test() {
		_authAndGetCsvD("1-MmRfRQ36K-QmFdcMuZOWmdZ3T67EnYtKkxXuRA5K7Q");
	}

	/**
	 * Create an OAuth2 client with the given credentials, and then execute the
	 * given callback function.
	 * @param {Object} credentials The authorization client credentials.
	 * @param {function} callback The callback to call with the authorized client.
	 */
	function _isAuthorizeOK(credentials) {
		const {
			client_secret,
			client_id,
			redirect_uris
		} = credentials.installed;

		oAuth2Client = new google.auth.OAuth2(
			client_id, client_secret, redirect_uris[0]);

		console.log("### googledrive: authorizing...");
		if (TOKEN) {
			oAuth2Client.setCredentials(TOKEN);
			return true;
		} else {
			console.log("token not found!");
			return false;
		}
	}

	/**
	 * Get and store new token after prompting for user authorization, and then
	 * execute the given callback with the authorized OAuth2 client.
	 * @param {google.auth.OAuth2} oAuth2Client The OAuth2 client to get token for.
	 * @param {getEventsCallback} callback The callback for the authorized client.
	 */
	function _getAccessTokenD() {
		const authUrl = oAuth2Client.generateAuthUrl({
			access_type: 'offline',
			scope: SCOPES,
		});
		console.log('Authorize this app by visiting this url:', authUrl);
		const rl = readline.createInterface({
			input: process.stdin,
			output: process.stdout,
		});

		return new Promise((resolve, reject) => {
			rl.question('Enter the code from that page here: ', (code) => {
				rl.close();
				oAuth2Client.getToken(code, (err, token) => {
					if (err) {
						console.error('Error retrieving access token', err);
						reject();
						return;
					}

					oAuth2Client.setCredentials(token);

					console.log("### New Token! Copy it to config.js\n", JSON.stringify(token));
				});
			});
		});
	}


	function _getCsvD(fileId) {
		const drive = google.drive({
			version: 'v3',
			auth: oAuth2Client
		});

		return new Promise((resolve, reject) => {
			console.log("### googledrive: fetching... fileId =", fileId);
			drive.files.export({
				fileId: fileId,
				mimeType: 'text/csv'
			}, function (err, response) {
				if (err) {
					console.log("#### error 0:", err);
					reject(err);
					return;
				}
				console.log("### googledrive: success! url =", response.config.url);
				resolve(response.data);
			});
		});
	}
}


module.exports = new GoogleDriveCsv();